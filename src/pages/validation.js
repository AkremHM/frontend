const validation = (values)=>{
    let regex = new RegExp('[a-z0-9]+@[a-z]+[a-z]{2,3}');
    let errors={"test1":"ok"};
    if(!values.firstname){
        errors.firstname="required"
        errors.test1=false;
    }
    if(!values.lastname){
        errors.lastname="required"
        errors.test1=false;
    }
    if(!values.email){
        errors.email="required"
        errors.test1=false;
    }else if (! regex.test(values.email))
    {
        errors.email="err"
        errors.test1=false;
    }
    if(!values.pwd){

        errors.pwd="required"
        errors.test1=false;
    }
    else if (values.pwd.length<5){
           errors.pwd="piw "
    errors.test1=false;
    }
 
    if(!values.pwdconf){
        errors.pwdconf="required"
        errors.test1=false;
    }else if(values.pwd !== values.pwdconf){
        errors.pwdconf="ghalet"
        errors.test1=false;
    }
    if(!values.age){
        errors.age="required"
        errors.test1=false;
    }
    if(errors.test1==="ok"){
        errors.test="true"
    }
    return errors;
};
export default validation;