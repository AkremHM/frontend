import axios from "axios";
import { useState } from "react";
import "./registration.css";
import validation  from "./validation";
function Register() {


const[errors,setErrors]=useState({});

const [values,setValues]= useState({
    firstname:"",
    lastname:"",
    email:"",
    pwd:"",
    pwdconf:"",
    age:""
});

const handleChange =(event)=>{
    setValues({...values,[event.target.name]:event.target.value,
    });
}

    async function handleSubmit(event) {
        event.preventDefault();
        let x = validation(values);
        setErrors(x);
                console.log(x);
     if(x.test==="true"){
        try {
            await axios.post("http://localhost:8080/Utilisateur/save1",
                {
             
                    firstName: values.firstname,
                    lastName: values.lastname,
                    email: values.email,
                    pwd: values.pwd,
                    pwdconf:values.pwdconf,
                    age: values.age
                });
            alert("User Registation Successfully");

        }
        catch (err) {
            alert("User Registation Failed");
        
        }
     }  
    }
    
    return (
        <div className="container" >


            <form className="register-form" onSubmit={handleSubmit}>
                <br></br>
                <div className="title">Inscription</div>
           

                <div className="input-box underline">
                    <input type="text"
                        name="firstname"
                        placeholder="Firstname"
                        onChange={handleChange}
                    />
                {errors.firstname && <p className="error">{errors.firstname}</p>}
                  
                    <div className="underline"></div>

                </div>

                <div className="input-box underline">

                    <input type="text"
                        name="lastname"
                        placeholder="lastname"
                        onChange={handleChange}
                    />
                   
                   {errors.lastname && <p className="error">{errors.lastname}</p>}
                    <div className="underline"></div>

                </div>

                <div className="input-box underline">
                    <input type="text"
                        name="email"
                        placeholder="Email"
                        onChange={handleChange}
                    />
                    {errors.email && <p className="error">{errors.email}</p>}
                    <div className="underline"></div>

                </div>

                <div className="input-box underline">
                    <input type="password"
                        name="pwd"
                        placeholder="Password"
                        onChange={handleChange}
                    />
                 {errors.pwd && <p className="error">{errors.pwd}</p>}
                    <div className="underline"></div>

                </div>
                <div className="input-box underline">
                    <input type="password"
                        name="pwdconf"
                        placeholder="Password confirm"
                        onChange={handleChange}
                    />
                   {errors.pwdconf && <p className="error">{errors.pwdconf}</p>}
                    <div className="underline"></div>

                </div>
                <div className="input-box underline">

                    <input type="number"
                        name="age"
                        placeholder="Age"
                        onChange={handleChange}
                    />
                     {errors.age && <p className="error">{errors.age}</p>}
                    <div className="underline"></div>

                </div>

                <div className="input-box button">
                    <button type="submit">Register</button>
                </div>

            </form>
        </div>
    )
    
}

export default Register;